<!doctype html>

<html lang="en">
<head>
	<meta charset="utf-8">

	<title>Stu Freen Design | Web and Graphic Design | Toronto</title>
  
	<meta name="description" content="Toronto-based Graphic Designer and Web Developer">
	<meta name="author" content="Stu Freen">
	<meta name="viewport" content="initial-scale=1.0">
  
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/swiper.min.css">
	<link rel="stylesheet" href="css/lightbox.css">
  
	<link rel="icon" type="image/x-icon" href="./favicon.ico">
	<meta name="theme-color" content="#b0635b">
	<link rel="Shortcut Icon" type="image/x-icon" href="./favicon.ico">
	<link rel="apple-touch-icon-precomposed" href="./images/icon-57.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="./images/icon-72.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="./images/icon-114.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="./images/icon-144.png">
  
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="./js/scrollReveal.min.js"></script>
	<script src="./js/swiper.jquery.min.js"></script>
	<script src="./js/sfscript.js"></script>
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
</head>

<?php include 'top.php';?>	
	<div class="work">
		<h2><a name="work">Work</a></h2>
			<?php
			$works = simplexml_load_file("./data/works.xml") or die("Error: Cannot create object");
			$count = 0;
			foreach($works->work as $daWork){
				echo "<a name='work-num-" . $count . "'>";
				echo "<div class='work-focus work-page-item'>";
				echo "<div class='container'>";
				if(!empty ($daWork->link) ){
					echo "<a href='" . $daWork->link . "' target='_blank'>";
					echo "<img src='./images/works/" . $daWork->bigthumb . "' alt='" . $daWork->shortdescription . " &ndash; " . $daWork->name . "'/>";
					echo "</a>";
				}
				else{
					echo "<a href='./images/works/" . $daWork->image . "' data-lightbox='" . $count ."' data-title='" . $daWork->name . "'>";
					echo "<img src='./images/works/" . $daWork->image . "' alt='" . $daWork->shortdescription . " &ndash; " . $daWork->name . "'/>";
					echo "</a>";
				}
				echo "<h3>" . $daWork->shortdescription . " &ndash; " . $daWork->name . "</h3>";
				echo "<p>" . $daWork->description;
				if(!empty ($daWork->link) ){
					echo "<a href='" . $daWork->link . "' target='_blank'> (Link)</a>";
				}
				echo "</p>";
				echo "</div>";
				echo "</div><!--end work-page-item-->";
				echo "</a>";
				$count++;
			}
			?>
	</div><!--end work-->
	
<?php include 'bottom.php';?>

  <script src="./js/lightbox.min.js"></script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66781633-1', 'auto');
  ga('send', 'pageview');

  </script>

</html>
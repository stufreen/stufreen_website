<!doctype html>

<html lang="en">
<head>
	<meta charset="utf-8">

	<title>Stu Freen Design | Web and Graphic Design | Toronto</title>
  
	<meta name="description" content="Toronto-based web and graphic design">
	<meta name="author" content="Stu Freen">
	<meta name="viewport" content="initial-scale=1.0">
	
	<meta property="og:title" content="Stu Freen | Web and Graphic Design" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="http://www.stufreen.com" />
	<meta property="og:image" content="http://www.stufreen.com/images/sflogo.jpg" />
	<meta property="og:description" content="Toronto-based web and graphic design." />
  
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/swiper.min.css">
	<link rel="stylesheet" href="css/lightbox.css">
  
	<link rel="icon" type="image/x-icon" href="./favicon.ico">
	<meta name="theme-color" content="#b0635b">
	<link rel="Shortcut Icon" type="image/x-icon" href="./favicon.ico">
	<link rel="apple-touch-icon-precomposed" href="./images/icon-57.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="./images/icon-72.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="./images/icon-114.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="./images/icon-144.png">
  
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="./js/scrollReveal.min.js"></script>
	<script src="./js/swiper.jquery.min.js"></script>
	<script src="./js/sfscript.js"></script>
	<script src="./js/bowser.min.js"></script>
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
</head>

<?php include 'top.php';?>	
	<div class="work">
		<h2><a name="work">Work</a></h2>	
		<div class="container">
			<div class="work-focus">
			
				<?php
				$works = simplexml_load_file("./data/works.xml") or die("Error: Cannot create object");
				if(!empty ($works->work[0]->link) ){
					echo "<a href='" . $works->work[0]->link . "' target='_blank'>";
					echo "<img src='./images/works/" . $works->work[0]->bigthumb . "' alt='" . $works->work[0]->shortdescription . " &ndash; " . $works->work[0]->name . "'/>";
					echo "</a>";
				}
				else{
					echo "<a href='" . $works->work[0]->image . "' target='_blank' data-lightbox='true' data-title='" . $works->work[0]->name . "'>";
					echo "<img src='./images/works/" . $works->work[0]->bigthumb . "' alt='" . $works->work[0]->shortdescription . " &ndash; " . $works->work[0]->name . "'/>";
					echo "</a>";
				}
				echo "<h3>" . $works->work[0]->shortdescription . " &ndash; " . $works->work[0]->name . "</h3>";
				echo "<p>" . $works->work[0]->description;
				if(!empty ($works->work[0]->link) ){
					echo "<a href='" . $works->work[0]->link . "' target='_blank'> (Link)</a>";
				}
				echo "</p>";

				?>
	
			</div><!--end work-focus--><div class="work-nav">
				<div class="javascript-disabled">
					<?php
					$count = 0;
					foreach($works->work as $daWork){
						echo "<a href='work.php#work-num-" . $count . "'>";
						echo "<div class='nav-work group' data-index='" . $count . "'>";
						echo "<div class='thumb-frame'><img src='./images/works/" . $daWork->thumbnail . "' alt='" . $daWork->shortdescription . " &ndash; " . $daWork->name . "'/></div>";
						echo "<h4>" . $daWork->shortdescription . "</h4>";
						echo "<p>" . $daWork->name . "</p>";
						echo "</div></a>";
						$count++;
					}
					?>
				</div><!--end javascript-disabled--><div class="javascript-enabled">
					<!-- Swiper -->
					<div class="swiper-container s0">
						<div class="swiper-wrapper">
							<?php
							$count = 0;
							foreach($works->work as $daWork){
								echo "<div class='swiper-slide'>";
								echo "<a href='#'>";
								echo "<div class='nav-work group' data-index='" . $count . "'>";
								echo "<div class='thumb-frame'><img src='./images/works/" . $daWork->thumbnail . "' alt='" . $daWork->shortdescription . " &ndash; " . $daWork->name . "'/></div>";
								echo "<h4>" . $daWork->shortdescription . "</h4>";
								echo "<p>" . $daWork->name . "</p>";
								echo "</div></a>";
								echo '</div>';
								$count++;
							}
							?>
						</div>
						<!-- Add Scroll Bar -->
						<div class="swiper-scrollbar"></div>
					</div><!--end swiper-container s0-->
				</div><!--end javascript-enabled-->
			</div><!--end work-nav--></div><!--end container-->
	</div><!--end work-->
	
	<div class="more-work">
		<div>
			<a href="work.php#work" class="show-more"><p class="more-link">See more work</p></a>
		</div>
	</div>
	
	<!-- <div class="experiments">
		<h2><a name="experiments">Experiments</a></h2>
		<div class="container">
			<p>Modern web browsers can do some pretty cool things. Your browser is up to snuff, so sit back and check out these little coding experiments. Think of them as ideas for future websites.</p>
			
			<div class="swiper-container s1">
					<div class="swiper-wrapper">
							<div class="swiper-slide">
								<p data-height="380" data-theme-id="0" data-slug-hash="gPoQqp" data-default-tab="result" data-user="stufreen" class='codepen'>See the Pen <a href='http://codepen.io/stufreen/pen/gPoQqp/'>Dropping notes</a> by Stu Freen (<a href='http://codepen.io/stufreen'>@stufreen</a>) on <a href='http://codepen.io'>CodePen</a>.</p>
							</div>
							<div class="swiper-slide">
								<p data-height="380" data-theme-id="0" data-slug-hash="epvvEN" data-default-tab="result" data-user="stufreen" class='codepen'>See the Pen <a href='http://codepen.io/stufreen/pen/epvvEN/'>Triangle swarm!</a> by Stu Freen (<a href='http://codepen.io/stufreen'>@stufreen</a>) on <a href='http://codepen.io'>CodePen</a>.</p>
							</div>
							<div class="swiper-slide">
								<p data-height="380" data-theme-id="0" data-slug-hash="vNybqG" data-default-tab="result" data-user="stufreen" class='codepen'>See the Pen <a href='http://codepen.io/stufreen/pen/vNybqG/'>Mighty morphin' form</a> by Stu Freen (<a href='http://codepen.io/stufreen'>@stufreen</a>) on <a href='http://codepen.io'>CodePen</a>.</p>
							</div>
							<script async src="//assets.codepen.io/assets/embed/ei.js"></script>
					</div>
					
					<div class="swiper-pagination"></div>
			</div>
		</div>
	</div>
-->

<?php include 'bottom.php';?>

  <script src="./js/lightbox.min.js"></script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66781633-1', 'auto');
  ga('send', 'pageview');

  </script>
  
</html>
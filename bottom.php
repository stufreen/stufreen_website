	<div class="about">
		<h2><a name="about">About</a></h2>
		<div class="container">
			<img data-sr src="./images/stu.png" alt="Stu Freen photo"/>
			<h3 data-sr='vFactor 0.9'>Stu Freen</h3>
			<a href="https://ca.linkedin.com/pub/stuart-freen/2/1a8/683" target="_blank"><img class="social-icon" src="./images/linkedin-icon.png" alt="Linked In icon"/></a>
			<a href="https://twitter.com/stafree" target="_blank"><img class="social-icon" src="./images/twitter-icon.png" alt="Twitter icon"/></a>
			<a href="https://www.flickr.com/photos/stafree" target="_blank"><img class="social-icon" src="./images/flickr-icon.png" alt="Flickr icon"/></a>
			<p data-sr='wait 0.2s scale 0%'>A jack of all trades. Stu left his job as a lawyer at a Bay Street law firm in 2015 to follow his passion for design. His undergrad degree in Computing forms the backbone of his programming skills, but that's just the start of it. Equally adept in print design, web development and back-end coding, he has the tools to meet your design needs.</p>
		</div><!--end container-->
	</div><!--end about-->
	
	<div class="contact">
		<h2><a name="contact">Contact</a></h2>
		<div class="container">
			<div data-sr='vFactor 0.3 wait 0.2s scale 0%' class="contact-details">
				<p class="contact-phone"><img src="./images/phone-icon.png" alt="Phone:"/>(416) 556-0723</p>
				<p class="contact-email"><img src="./images/email-icon.png" alt="Email:"/>stufreen@gmail.com</p>
			</div>
			<div class="contact-form">
				<p class="form-title"><a name="form">Get a quote for your project:</a></p>
				<?php
				// define variables and set to empty values
				$nameErr = $emailErr = $descriptionErr = "";
				$name = $email = $description = "";

				if ($_SERVER["REQUEST_METHOD"] == "POST") {
					if (empty($_POST["name"])) {
						$nameErr = "Name is required";
					} else {
						$name = test_input($_POST["name"]);
						// check if name only contains letters and whitespace
						if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
							$nameErr = "Only letters and white space allowed"; 
						}
					}

					if (empty($_POST["email"])) {
						$emailErr = "Email is required";
					} else {
						$email = test_input($_POST["email"]);
						// check if e-mail address is well-formed
						if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
							$emailErr = "Invalid email format"; 
						}
					}

					if (empty($_POST["description"])) {
						$descriptionErr = "Description is required";
					} else {
						$description = test_input($_POST["description"]);
					}
					
					if($nameErr == "" &&
						$emailErr == "" &&
						$descriptionErr == ""){
						echo '<script type="text/javascript">showOverlay()</script>';
						mail("stufreen@gmail.com", "Message from stufreen.com", "The following message was sent by " . $name . " (" . $email . "): " . $description);
						//echo "The following message was sent by " . $name . " (" . $email . "): " . $description;
					}
				}

				function test_input($data) {
					$data = trim($data);
					$data = stripslashes($data);
					$data = htmlspecialchars($data);
					return $data;
				}
				?>
			
				<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>#form">
					<fieldset>
						<label for="name">Name</label>
						<input type="text" name="name"/>&nbsp;* 
					</fieldset>
					<p class="form-error"><?php echo $nameErr;?></p>
					
					<fieldset>
						<label for="email">E-mail</label>
						<input type="text" name="email"/>&nbsp;* 
					</fieldset>
					<p class="form-error"><?php echo $emailErr;?></p>
					
					<fieldset>
						<label for="description">Describe your project</label>
						<textarea type="text" name="description" id="description"></textarea>&nbsp;*
					</fieldset>
					<p class="form-error"><?php echo $descriptionErr;?></p>
					
					<fieldset>
						<label for="submit">&nbsp;</label>
						<input type="submit" name="submit" id="submit" value='Send'/>
					</fieldset>
				</form>
			</div><!--end contact-form-->
		</div><!--end container-->
	</div><!--end contact-->
	
	<div class="success-overlay"></div><!--end success-overlay-->
	<div class="success-message">
		<p>Thanks! Your message was sent. Stu will get back to you shortly.</p>
		<a href="#">Okay</a>
	</div><!--end success-message-->
	
</body>
<body>
	<div class="header">
		<div class="container">
			<a href="./"><img src="images/header2.png" alt="Stu Freen Design" id="banner"></a>
		</div><!--end container-->
	</div><!--end header-->
	
	<div class="menu">
		<div class="container">
			<ul>
				<a href="#work"><li>Work</li></a><a href="#about"><li>About</li></a><a href="#contact"><li>Contact</li></a>
			</ul>
		</div><!--end container-->
	</div><!--end header-->
	
	<div class="blurb">
		<div class="container">
			<div class="quote">
				<h4><i>Quality design</i><br/> is something you can feel.</h4>
				<p>Mid-to-small sized businesses don’t necessarily need to hire an expensive media company to get quality graphic and web design services. Often you just need one go-to guy that you can depend on.</p>
			</div>
		</div><!--end container-->
	</div><!--end blurb-->
	
$(document).ready(function(){
	
	//Function for parallax background scrolling in header
    $('.header').each(function(){
        var $bgobj = $(this),
			$window = $(window);
    
        $window.scroll(function() {
			
			var windowScroll = $window.scrollTop();
			
			if (windowScroll < 0){
				windowScroll = 0;
			}
			
            var yPos = -(windowScroll / 2.8); //the value here defines how fast the scrolling should be
            
            // Put together our final background position
            var coords = '50% '+ yPos + 'px';

            // Move the background
            $bgobj.css({ backgroundPosition: coords });
        }); 
    });
	
	//Function for menu to follow the scrolling and always appear at the top of the page
	
	/*
	$('.menu').each(function(){
		var $menu   = $(this), 
			$window    = $(window),
			offset     = $menu.offset(),
			topPadding = 0;

		$window.scroll(function() {
			if ($window.scrollTop() > offset.top) {
				//$menu.stop().animate({
				//	marginTop: $window.scrollTop() - offset.top + topPadding
				//});
				$menu.css('margin-top', $window.scrollTop() - offset.top + topPadding);
			} else {
				// $menu.stop().animate({
					// marginTop: 0
				// });
				$menu.css('margin-top', '0');
			}
		});
		
		$window.resize(function() {
			//This is needed because page is responsive. Menu offset can change on resize.
			offset = $menu.offset();
		});
    });
	*/
	
	//Function for menu links to smooth scroll to the desired anchor
	$('div.menu a').click(function(){
		var target = $(this).attr('href');
		target = target.substr(1);
		var $aTag = $("a[name='" + target + "']");
		var menuHeight = $('div.menu').height();
		
		$('html, body').animate({scrollTop: $aTag.offset().top /*- menuHeight*/}, 1000, "easeInOutQuad");
		return false;
	});
	
	//Following code reads in the xml file on the client side
	var workCount = 0;
	var workImages = new Array();
	var workBigthumbs = new Array();
	var workLinks = new Array();
	var workNames = new Array();
	var workDescriptions = new Array();
	var workShortDescriptions = new Array();
	
	$.get('./data/works.xml', function(d){
		$(d).find('work').each(function(){
            var $work = $(this);
            workImages[workCount] = $work.find('image').text();
			workBigthumbs[workCount] = $work.find('bigthumb').text();
			workLinks[workCount] = $work.find('link').text();
            workNames[workCount] = $work.find('name').text();
            workDescriptions[workCount] = $work.find('description').text();
			workShortDescriptions[workCount] = $work.find('shortdescription').text();
			workCount++;
        });
		workCount = 1;
    });
	
	//Following code is for "show more work" link in small layouts
	$('a.show-more').click(function(){
		
		var toInsert = "<div class='work-focus'>";
		
		if(workLinks[workCount] != ""){
			toInsert += "<a href='" + workLinks[workCount] + "' target='_blank'>";
			toInsert += "<img src='./images/works/" + workBigthumbs[workCount] + "'/></a>";
		}
		else{
			toInsert += "<a href='./images/works/" + workBigthumbs[workCount] + "' target='_blank' data-lightbox='true'>";
			toInsert += "<img src='./images/works/" + workBigthumbs[workCount] + "'/></a>";
		}
		toInsert += "<h3>" + workShortDescriptions[workCount] + " &ndash; " + workNames[workCount] + "</h3>";
		toInsert += "<p>" + workDescriptions[workCount];
		if(workLinks[workCount] != ""){
			toInsert += "<a href='" + workLinks[workCount] + "' target='_blank'> (Link)</a>";
		}
		toInsert += "</p></div>";
		
		var toInsertHTML = $.parseHTML(toInsert);
		
		$(this).before(toInsertHTML);
		
		workCount++;
		if(workCount >= workImages.length){
			$(this).remove();
		}
		
		return false;
	});
	
 	//Following code replaces the javascript-disabled work navigator with a nice swiper version
	$('.javascript-disabled').each(function(){
		if (!bowser.msie || (bowser.msie && bowser.version >= 9)) {
			$(this).remove();
			$('.javascript-enabled').css('display', 'inline');
			
			var swiper0 = new Swiper('.s0.swiper-container', {
				scrollbar: '.swiper-scrollbar',
				direction: 'vertical',
				slidesPerView: 4,
				paginationClickable: true,
				spaceBetween: 0,
				mousewheelControl: true,
				freeMode: true
			});
		}
	});
	
	//Following code replaces the highlighted work when a thumbnail is clicked
	$('div.nav-work').click(function(){
		var index = $(this).data("index");
		$('div.work div.work-focus img').attr('src', './images/works/'+workBigthumbs[index]);
		$('div.work div.work-focus img').attr('alt', workShortDescriptions[index] + " - " + workNames[index] );
		$('div.work div.work-focus h3').replaceWith("<h3>"+ workShortDescriptions[index] + " &ndash; " + workNames[index] +"</h3>");
		$('div.work div.work-focus p').replaceWith("<p>"+workDescriptions[index]+"</p>");
		if(workLinks[index] != ""){
			$('div.work div.work-focus a').attr('href', workLinks[index]);
			$('div.work div.work-focus a').removeAttr('data-lightbox', 'data-title');
			$('div.work div.work-focus p').append("<a href='"+workLinks[index]+"' target='_blank'> (Link)</a>");
		}
		else{
			$('div.work div.work-focus a').attr('href', './images/works/'+workImages[index]);
			$('div.work div.work-focus a').attr('data-lightbox', index);
			$('div.work div.work-focus a').attr('data-title', workNames[index]);
		}
		return false;
	}); 
	
	//Creates scrollReveal instance
	
	var srconfig = {
        mobile: true
    }
	window.sr = new scrollReveal(srconfig);

	//hide overlay if you click the black part or the okay link
	$('div.success-overlay').click(function(){
		$('div.success-overlay').stop(true);//stop automatic fade out
		$('div.success-message').stop(true);
		$('div.success-overlay').fadeOut('fast');
		$('div.success-message').fadeOut('fast');
	})
	
	$('div.success-message a').click(function(){
		$('div.success-overlay').stop(true);
		$('div.success-message').stop(true);
		$('div.success-overlay').fadeOut('fast');
		$('div.success-message').fadeOut('fast');
		return false;
	})
	
	//Show experiments if browser supports JS and CSS3
	if (bowser.msedge ||
			bowser.msie && bowser.version >= 10 ||
			bowser.safari  && bowser.version >= 9 ||
			bowser.chrome  && bowser.version >= 43 ||
			bowser.firefox  && bowser.version >= 28 ||
			bowser.opera  && bowser.version >= 30) {
				
		if(!bowser.mobile){
			$('div.experiments').show();
			var swiper1 = new Swiper('.s1.swiper-container', {
				pagination: '.swiper-pagination',
				paginationClickable: true
			});
		}
	}
});

//Runs on full page load
$(window).bind("load", function() {
	//Show overlay if overlay is true;
	if($overlay==true){
		//$('div.success-overlay').css('top', $(window).scrollTop());
		//$('div.success-message').css('margin-top', $(window).scrollTop()-60);
		$('div.success-overlay').css('display', 'block');
		$('div.success-message').css('display', 'block');
		$('div.success-overlay').delay(6000).fadeOut(2000);
		$('div.success-message').delay(6000).fadeOut(2000);
	}
});

var $overlay = false;
function showOverlay() {
	$overlay = true;
}